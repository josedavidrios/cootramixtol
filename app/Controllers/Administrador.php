<?php namespace App\Controllers;

use App\Models\DespachoModel;
use App\Models\TarifaModel;
use App\Models\VehiculoAsociadoModel;
use App\Models\VehiculoModel;

class Administrador extends BaseController
{

	public $vehiculoModel;
	public $tarifaModel;

	public function __construct()
	{
		helper("time");
		helper(['html']);
		$this->vehiculoModel = new VehiculoModel();
		$this->tarifaModel = new TarifaModel();

	}



	function vistaTarifa()
	{
		$dat['content'] = "tarifa/inicio";
		$dat['title'] = "Crear y listar tarifas";
		echo view('dashboard/inc/layout', $dat);

	}

	function vistaAsignarTarifa()
	{
		$dat['content'] = "tarifa/Asignar_tarifas_vehiculos.php";
		$dat['title'] = "Asignar tarifas a vehículos";

		$dat['css'] = ['vue-select.css'];
		$dat['js'] = ['lodash.min.js', 'vue-select.js', 'app/tarifas/asignarTarifas.js'];

		echo view('dashboard/inc/layout', $dat);

	}

	function index()
	{


		$dat['css'] = [''];
		$dat['title'] = "Inicio";




		if (strcmp(session("rol"),"ADMIN")==0){

			$dat['content'] = "vigencia_vehiculos";
			$dat['data'] = $this->vehiculoModel->consultarVigenciasVehiculos();
		}else{

			$dat['content'] = "blank";
		}



		echo view('dashboard/inc/layout', $dat);

	}


	/*
	 *
	 * Vistas
	 *
	 * */




	function vistaVehiculos()
	{

		$dat['css'] = [''];
		$dat['title'] = "Vehículos";
		$dat['content'] = "vehiculo/inicio";


		echo view('dashboard/inc/layout', $dat);

	}


	function vistaConductores()
	{


		$dat['css'] = ['switchery.min.css'];
        $dat['js'] = ['switchery.min.js'];


        $dat['title'] = "Conductores";
		$dat['content'] = "conductor/inicio";


		echo view('dashboard/inc/layout', $dat);

	}


	function vistaAsociados()
	{
		$dat['css'] = [''];
		$dat['title'] = "Asociados";
		$dat['content'] = "asociado/inicio";


		echo view('dashboard/inc/layout', $dat);

	}


	function vistaPlanillas()
	{

		$dat['css'] = [''];
		$dat['title'] = "Planillas";
		$dat['content'] = "planilla/planillado_general";


		echo view('dashboard/inc/layout', $dat);

	}


	function vistaPlanillasEspecificas()
	{

		helper(['html']);

		$dat['css'] = ['vue-select.css'];
		$dat['js'] = ['lodash.min.js', 'vue-select.js', 'app/planilla/planilla.js'];

		$dat['title'] = "Planillas";
		$dat['content'] = "planilla/planillado_especifico";

		//   $dat['placas'] =


		echo view('dashboard/inc/layout', $dat);

	}

	function vistaPlanillasIndibivuales()
	{

		helper(['html']);

		$dat['css'] = ['vue-select.css'];
		$dat['js'] = ['lodash.min.js', 'vue-select.js', 'app/planilla/planillaIndibidual.js'];

		$dat['title'] = "Planillas";
		$dat['content'] = "planilla/planillado_individual";

		//   $dat['placas'] =


		echo view('dashboard/inc/layout', $dat);

	}

	function vistaDescargarPlanilla()
	{

		$dat['css'] = ['vue-select.css'];

		$dat['title'] = "Planillas";
		$dat['content'] = "planilla/descargar";
		$dat['js'] = ['lodash.min.js', 'vue-select.js', 'app/planilla/descargarPlanillas.js'];

		//   $dat['placas'] =


		echo view('dashboard/inc/layout', $dat);

	}


	function vistaRegistrarDespachos()
	{


		$dat['css'] = [''];
		$dat['title'] = "Despacho de Vehículos";
		$dat['content'] = "despacho/registro";


		echo view('dashboard/inc/layout', $dat);

	}

	function vistaConsultarDespachosEntreFechas()
	{


		$dat['css'] = ['vue-select.css'];

		$dat['title'] = "Consultar Despacho de Vehículos";
		$dat['content'] = "despacho/consultar_entre_fechas";
		$dat['js'] = ['lodash.min.js', 'vue-select.js', 'app/despacho/consultarEntrefechas.js'];

		echo view('dashboard/inc/layout', $dat);

	}



	function VistaCambiarClave()
	{


		$dat['css'] = ['vue-select.css'];

		$dat['title'] = "Cambiar clave";
		$dat['content'] = "cambioClave/cambiarClave";
		$dat['js'] = [];

		echo view('dashboard/inc/layout', $dat);

	}

	function vistaConsultarDespachosDia()
	{


		$dat['css'] = ['vue-select.css'];

		$dat['title'] = "Consultar Despacho de Vehículos";
		$dat['content'] = "despacho/consultar_dia";
		$dat['js'] = ['lodash.min.js', 'vue-select.js', 'app/despacho/consultar.js'];


		$dat['placas']  = ["PX","xx"];


		if (strcmp(session("rol"),"ADMIN")==0){

			$dat['placas'] =  $this->vehiculoModel->select("placa AS placaVehiculo")->findAll();

		}else {

			$vehiculosAsociadosModel = new VehiculoAsociadoModel();
			$dat['placas']=$vehiculosAsociadosModel->consultarVehiculosPorAsociado(session("identificacion"));

		}


		echo view('dashboard/inc/layout', $dat);

	}


}
