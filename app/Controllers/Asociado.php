<?php namespace App\Controllers;

use App\Models\UsuarioModel;




class Asociado extends BaseController
{
    public $asociadoModel;
    public function __construct()
    {

        $this->asociadoModel = new UsuarioModel();

    }




    /*
    function consultar(){



        $placa =  $this->request->getPost("identificacion");

      return  json_encode($this->conductoresModel->consultar($placa));


    }

    */


    function filtrar(){

		$nombres =  $this->request->getPost("nombres");

		if (strlen($nombres)>1){

			$filtro = $this->asociadoModel->filtrar($nombres);

			/*
            foreach ($filtro as $fil) {
                $fil->activo = badge_si_no($fil->activo);
            }

			*/
            echo json_encode($filtro);
		}




	}

	function consultar($identificacion=null)
	{

		return $this->asociadoModel->consultar($identificacion);


	}

    function mostrar(){

        $identificacion =  $this->request->getGetPost("identificacion");
        $asociados =  $this->asociadoModel->consultar($identificacion);
        echo json_encode($asociados);

    }

	function mostrarTodos(){


		$asociados =  $this->asociadoModel->consultarTodos();
		echo json_encode($asociados);

	}


    function crear(){

        $datos= get_post();
		$existe = $this->consultar($datos["identificacion"]);

		if (is_null($existe)){
			$datos['activo'] = 1;
			$datos["rol"]="ASOC";
			$datos['fechaRegistro'] = get_now();
		}else {

			$datos['ultimaFechaActualizacion'] = get_now();
		}
		echo $this->asociadoModel->save($datos);

    }








}
