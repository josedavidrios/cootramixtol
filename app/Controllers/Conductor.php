<?php namespace App\Controllers;


use App\Models\ConductorModel;


class Conductor extends BaseController
{
	public $conductorModel;

	public function __construct()
	{
		$this->conductorModel = new ConductorModel();

	}

	function consultar()
	{

		$identificacion = $this->request->getGetPost("identificacion");
		$activo = $this->request->getGetPost("activo");


		$conductores = $this->conductorModel->consultar($identificacion,$activo);

		echo json_encode($conductores);

	}


	function filtrar()
	{


		$nombres = $this->request->getPost("nombres");


		if (strlen($nombres) > 1) {


			$filtro = $this->conductorModel->filtrar($nombres);


			echo json_encode($filtro);

		}


	}


	function mostrar()
	{


		$conductores = $this->conductorModel->consultarTodo();

		echo json_encode($conductores);

	}


	function crear()
	{

		$datos = get_post();
		$existe = $this->conductorModel->consultar($datos["identificacion"]);

		if (is_null($existe)) {
			$datos['activo'] = 1;
			$datos['fechaRegistro'] = get_now();

		}else{
			$datos['ultimaFechaActualizacion'] = get_now();
		}

		echo $this->conductorModel->save($datos);




	}


}
