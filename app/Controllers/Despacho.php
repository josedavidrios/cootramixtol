<?php namespace App\Controllers;


use App\Models\DespachoModel;
use App\Models\PlanillaModel;


class Despacho extends BaseController
{

    private $despachoModel;
	private $planillaModel;
    public function __construct()
    {
        $this->despachoModel = new DespachoModel();
		$this->planillaModel = new PlanillaModel();

    }




    function crear(){

        $datos= get_post();


		$planillas = $this->planillaModel->consultarPorPlaca($datos['placaVehiculo'],$datos['fecha']);

		if (count($planillas)>0){

			$datos['fechaRegistro']=get_now();
			$this->despachoModel->save($datos);
			echo 1;
		}


		echo 0;


    }


    function consultar(){

        $placa = $this->request->getGetPost("placa");
        $fecha = $this->request->getGetPost("fecha");
        echo json_encode($this->despachoModel->consultar($placa,$fecha));

    }

	function consultarPorFecha(){

		$placa = $this->request->getGetPost("placa");
		$fechaInicio = $this->request->getGetPost("fechaInicio");
		$fechaFin = $this->request->getGetPost("fechaFin");
		echo json_encode($this->despachoModel->consultarPorFechas($placa,$fechaInicio,$fechaFin));

	}


}
