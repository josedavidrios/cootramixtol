<?php

namespace App\Controllers;
use App\Models\PlanillaModel;
use App\Libraries\Pdf;
class Generar extends  BaseController
{

	public $planillaModel;
	public function __construct()
	{
		$this->planillaModel = new PlanillaModel();

	}

	function x(){

		echo "Generar";
	}

	function planilla()
	{


		$placa = $this->request->getGetPost('placa');
		$fecha = $this->request->getGetPost('fecha');


		$planillas = $this->planillaModel->consultarPorPlaca( $placa,$fecha);


		header('Content-type: application/pdf');

		$pdf = new Pdf('P', 'cm', [8, 10]);
		$pdf->SetTitle("PLANILLA DE VIAJE");

		foreach ($planillas as $planilla) {


			$pdf->AddPage();


			$pdf->SetFont('Arial', '', 10);

			//	$pdf->Image(base_url('azzara/assets/img/logo-supertransporte.png'), 1, 1, 5100 / 1000, 2800 / 1000, "PNG", "");


			$pdf->Image(base_url('azzara/assets/img/cooperativa2.png'), 0, 0, 1220 / 150, 362 / 150, "PNG", "");

			$pdf->SetXY(0.8, 2.2);
			$pdf->Cell(1, 1, 'NIT: 823003263-1 | TEL: 3016879718');
			$pdf->SetXY(0.85, 2.8);
			$pdf->SetFont('Arial', '', 9);
			$pdf->Cell(1, 1, 'DIAGONAL 2 6 93, TOLUVIEJO, SUCRE');



			$pdf->SetFont('Arial', '', 10);

			$pdf->Ln(1);
			$pdf->Cell(0, 0, 'PLANILLA DE VIAJE ' . utf8_decode('N° ' . str_pad($planilla->numero, 5, "0", STR_PAD_LEFT)),0,1,'C');
			$pdf->SetFont('Arial', '', 9);
			$pdf->Ln(1);

			$pdf->Cell(0, 0, 'FECHA: ' . $planilla->fecha,0,1,'C');


			$pdf->Ln(0.5);

			$pdf->Cell(0, 0, 'RUTA: '.$planilla->ruta, 0, 1, 'C');


			$pdf->Ln(0.5);

			$pdf->Cell(0, 0, utf8_decode('PLACA '.$planilla->placaVehiculo), 0, 1, 'C');

			$pdf->Ln(1);

			$pdf->Cell(0, 0, 'CONDUCTOR PRINCIPAL', 0, 1, 'C');

			$pdf->Ln(0.5);



			$pdf->Cell(0, 0, $planilla->conductor, 0, 1, 'C');

			$pdf->Ln(0.5);
			$pdf->Cell(0, 0, "-------------------------------------------------------------------", 0, 1, 'C');
			// $pdf->Cell(1,1,'');


		}


		$pdf->Output();

		exit;


	}
}
