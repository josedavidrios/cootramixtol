<?php namespace App\Controllers;

use App\Models\PlanillaModel;


class Planilla extends BaseController
{


	public $planillaModel;

	public function __construct()
	{
		$this->planillaModel = new PlanillaModel();

	}


	function filtrar()
	{
		$placa = $this->request->getPost("placa");

		if (strlen($placa) > 1) {

			$filtro = $this->planillaModel->filtrar($placa);
			echo json_encode($filtro);

		}
	}

	function cambiarEstado()
	{
		$numero = $this->request->getGetPost("numero");
		$estado = $this->request->getGetPost("estado");
		return $this->planillaModel->cambiarEstado($numero,$estado);
	}


	function consultar()
	{
		$codigo = $this->request->getGetPost("placa");
		$usuarios = $this->planillaModel->consultar($codigo);

		echo json_encode($usuarios);

	}


	function crearDetallePlanillas($numero){


        $rutas =["ST","TS"];

        for ($i=0;$i<count($rutas);$i++){


            $datosDetalleNuevaPlanilla=[

                "numeroPlanilla"=>$numero,
                "codigoRuta"=>$rutas[$i]

            ];

            $this->planillaModel->crearDetallePlanilla($datosDetalleNuevaPlanilla);


        }




    }



	function crear()
	{
		$cantPlanillas = 0;

		$datos = get_post();
		$datos['fechaRegistro'] = get_now();
		$fechaInicio = strtotime($datos['fechaInicio']);
		$fechas = [];
        $rutas =["ST","TS"];

		if (isset($datos['fechaFin'])) {

			$fechaFin = strtotime($datos['fechaFin']);

			for ($i = $fechaInicio; $i <= $fechaFin; $i += 86400) {

				$aux = date("Y-m-d", $i);
				array_push($fechas, $aux);

			}

		} else {

			$fechas = [$datos['fechaInicio']];

		}


		$vehiculo = new Vehiculo();

		for ($i = 0; $i < count($fechas); $i++) {

			foreach ($vehiculo->consultarHabilitados() as $aux) {

				$datosNuevaPlanilla = [

					"fecha" => $fechas[$i],
					"identificacionConductor" => $aux->identificacionConductorTitular,
					"placaVehiculo" => $aux->placa,
					"codigoTarifa" => $aux->codigoTarifa,
					"estado" => 0,
					"fechaRegistro" => get_now(),
					"registradoPor"=> intval(session('identificacion'))


				];

                $numero=	$this->planillaModel->crear($datosNuevaPlanilla);
               	$this->crearDetallePlanillas($numero);

				$cantPlanillas++;

            }

		}

		echo json_encode($cantPlanillas);
	}

	function individual()
	{

		$datos = get_post();

		$vehiculo = new Vehiculo();

		$aux = $vehiculo->consultar($datos['placa']);

		$tarifa = $aux->codigoTarifa;
		$estado = 0;



		if (strcmp($datos['tarifaCero'],"TRUE")==0){
			$tarifa=0;
			$estado=1;

		}

		$datosNuevaPlanilla = [

			"fecha" => $datos['fecha'],
			"identificacionConductor" => $datos['conductor'],
			"placaVehiculo" => $aux->placa,
			"codigoTarifa" => $tarifa,
			"estado" => $estado,
			"fechaRegistro" => get_now(),
			"registradoPor"=>session('identificacion')


		];

		$numero = $this->planillaModel->crear($datosNuevaPlanilla);
		$this->crearDetallePlanillas($numero);


		echo  $numero;
	}


	function crear2()
	{


		$datos = get_post();
		$placas = explode(",", $datos['placas']);

		$cant = 0;


		foreach ($placas as $placa) {

			$vehiculo = new Vehiculo();


		$aux = $vehiculo->consultar($placa);

					$datosNuevaPlanilla = [

						"fecha" => $datos['fecha'],
						"identificacionConductor" => $aux->identificacionConductorTitular,
						"placaVehiculo" => $aux->placa,
						"codigoTarifa" => $aux->codigoTarifa,
						"pagada" => 0,
						"fechaRegistro" => get_now(),
						"registradoPor"=>session('identificacion')


					];


					$numero = $this->planillaModel->crear($datosNuevaPlanilla);
					$this->crearDetallePlanillas($numero);

					$cant++;



	}


		echo $cant;

	}



}
