<?php namespace App\Controllers;

use App\Models\SesionModel;
use App\Models\UsuarioModel;


class Sesion extends BaseController
{

    public $sesion;
    public  $modeloSesion;
	public  $modeloUsuario;

    public function __construct()
    {


        $this->modeloSesion = new SesionModel();
        $this->sesion = $session = \Config\Services::session();
		$this->modeloUsuario = new UsuarioModel();


    }



	function cambiarClave(){


		$claveAnterior =  $this->request->getPost("claveAnterior");
		$nuevaClave =  $this->request->getPost("nuevaClave");

		$usuario = intval(session("identificacion")) ;

		$admin = $this->modeloSesion->consultarDatos($usuario);

			if (password_verify($claveAnterior,$admin->clave)){

				$nuevaClave = password_hash($nuevaClave, PASSWORD_BCRYPT);

				if($this->modeloUsuario->cambiarClave($usuario,$nuevaClave)){

					$email = \Config\Services::email();

					$email->setFrom('sipd.cootramixtol@gmail.com', 'SIPD - COOTRAMIXTOL');
				//	$email->setTo($admin->correo);
					$email->setTo("joseriospacheco@gmail.com");
					$email->setSubject('Asunto del Correo');
					$email->setMessage('Contenido del Correo');

					if ($email->send()) {
						echo 'Correo enviado exitosamente.';
					} else {
						echo $email->printDebugger(['headers']);
					}


					/*
					$mjs = "Hola, $admin->nombres,
								Este es un mensaje para confirmar que has realizado exitosamente el cambio de tu contraseña. Si realizaste este cambio, no es necesario que realices ninguna otra acción. En caso de que no hayas sido tú quien realizó este cambio, por favor contáctanos de inmediato.
								Mantén tu contraseña segura y no la compartas con nadie. Si necesitas ayuda o tienes alguna pregunta, no dudes en ponerte en contacto con nosotros.
								Saludos cordiales,

								SIPD - Cootramixtol";

					$email->setFrom('sipd.cootramixtol@gmail.com', 'SIPD - Cootramixtol');
					$email->setTo($admin->correo);
					$email->setSubject('Cambio de clave - SIPD - Cootramixtol');
					$email->setMessage($mjs);

					if ($email->send()) {
						echo 'Correo enviado exitosamente.';
					}


					*/

				}





			}
	}

    public function index()
	{



        if ($this->sesion->has('identificacion')) {

            return redirect()->to(base_url('Administrador'));


        }else{

            $dat['title'] = "Dashboard";
            echo view('sesion/login', $dat);


        }


    }



    function iniciar(){

        $usuario = $this->request->getPost('usuario');
        $clave = $this->request->getPost('clave');
        $admin = $this->modeloSesion->consultarDatos($usuario);

        if (!is_null($admin)){

			if (password_verify($clave,$admin->clave)){

				$datosUsuarios = [
					"identificacion" => $admin->identificacion,
					"rol" => $admin->rol,
					"nombres"=>$admin->nombres,
					"nombreRol"=>$admin->nombreRol
				];

				$this->sesion->set($datosUsuarios);
				return redirect()->to(base_url("Administrador"));

			}

        }
            $this->sesion->setTempdata( 'mensaje_login',1 ,  300 );
			return redirect()->to(base_url());


    }


    function cerrar(){

        session_destroy();
        return redirect()->to(base_url());

    }

}
