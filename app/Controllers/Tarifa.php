<?php

namespace App\Controllers;

use App\Models\TarifaModel;
use App\Models\VehiculoModel;

class Tarifa extends BaseController
{

	private  $tarifaModel;
	private  $vehiculoModel;
	public function __construct()
	{
		$this->tarifaModel = new TarifaModel();
		$this->vehiculoModel = new VehiculoModel();
	}


	function crear()
	{

		$datos = get_post();
		$datos['fechaRegistro'] = get_now();

		$datos['total']= doubleval($datos['gastosAdministracion'])
			            +doubleval($datos['aporteSocial'])
			            +doubleval($datos['fondoReposicion'])
			            +doubleval($datos['aportesGastosVarios'])
						+doubleval($datos['aporteParqueadero']);

		$this->tarifaModel->insert($datos);
		$id = $this->tarifaModel->getInsertID();

		if (strcmp($datos['autoAplicar'],"TRUE")==0){
			$this->cambiarTarifaMasiva($id);
		}

	}

	function asignarTarifaVehiculos(){


		$datos = get_post();
		$cant = 0;


		if (strcmp($datos['todos'],"TRUE")==0){

			$cant =	$this->cambiarTarifaMasiva($datos['tarifa']);

		}else {


			$placas = explode(",", $datos['placas']);

			foreach ($placas as $placa) {

				$this->vehiculoModel->cambiarTarifa($placa,$datos['tarifa']);

				$cant++;
			}
		}

		echo  $cant;
	}

	function cambiarTarifaMasiva($tarifa){

		$cant =1;

		foreach ($this->vehiculoModel->consultarActivos() as $aux) {

			$this->vehiculoModel->cambiarTarifa($aux->placa,$tarifa);
			$cant++;
		}

		return $cant;
	}


	function mostrarTipos()
	{
		$filtro = $this->tarifaModel->consultarTarifasAnioActual();
		echo json_encode($filtro);

	}

	function inactivar(){

		$codigo = $this->request->getPost("codigo");
		$this->tarifaModel->inactivar($codigo);

	}

	function listar()
	{
		$filtro = $this->tarifaModel->findAll();
		echo json_encode($filtro);

	}

}
