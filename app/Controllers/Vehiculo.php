<?php namespace App\Controllers;


use App\Models\VehiculoAsociadoModel;
use App\Models\VehiculoModel;


class Vehiculo extends BaseController
{


	public $vehiculoModel;
	public function __construct()
	{
		$this->vehiculoModel = new VehiculoModel();
	}


	function filtrar()
	{


		$placa = $this->request->getPost("placa");


		if (strlen($placa) > 2) {


			$filtro = $this->vehiculoModel->filtrar($placa);


			echo json_encode($filtro);

		}


	}


	function mostrar()
	{
		$placa = $this->request->getPost("placa");
		$vehiculos = $this->vehiculoModel->consultarDetalles($placa);
		echo json_encode($vehiculos);

	}


	function consultarInformacionConductor()
	{


		$placa = $this->request->getGetPost("placa");


		$vehiculos = $this->vehiculoModel->consultarInformacionConductor($placa);

		echo json_encode($vehiculos);

	}


	function consultarHabilitados()
	{
		return $this->vehiculoModel->consultarHabilitados();
	}

	function consultar($placa=null)
	{
		return $this->vehiculoModel->consultar($placa);


	}


	function mostrarPlacas()
	{
		$array =[];

		if (strcmp(session("rol"),"ADMIN")==0){

		//	echo json_encode($this->vehiculoModel->select("codigo as value,placa as label")->findAll());
		//	echo json_encode($this->vehiculoModel->select("placa")->findAll());

			$aux = $this->vehiculoModel->select("placa")->where("activo",1)->findAll();

			foreach ($aux as $a){

				array_push($array,$a->placa);
			}

			echo json_encode($array);

		}else {

			$vehiculosAsociadosModel = new VehiculoAsociadoModel();


			$aux = $vehiculosAsociadosModel->consultarVehiculosPorAsociado(session("identificacion"));



			foreach ($aux as $a){

				array_push($array,$a->placaVehiculo);
			}

			echo json_encode($array);

		}



	}


	function crear()
	{

		$datos = get_post();

		$existe = $this->vehiculoModel->existe($datos['placa']);
		$existe > 0 ? $datos['ultimaFechaActualizacion'] = get_now() : $datos['fechaRegistro'] = get_now();

		echo $this->vehiculoModel->save($datos);


	}


	function xxx()
	{
		echo $this->vehiculoModel->existe("zzz");


	}


}
