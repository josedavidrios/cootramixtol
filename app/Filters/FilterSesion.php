<?php namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;
use Config\Services;

class FilterSesion implements FilterInterface
{



	public function before(RequestInterface $request)
	{

		if (is_null(session('identificacion'))) {
			return redirect()->to(base_url());  // Redirigir al login si no está autenticado
		}

	}


	public function after(RequestInterface $request, ResponseInterface $response)
	{
		// TODO: Implement after() method.
	}
}
