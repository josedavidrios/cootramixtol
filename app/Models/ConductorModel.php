<?php
/**
 * Created by PhpStorm.
 * Administrador: jose.riosp
 * Date: 26/11/2019
 * Time: 10:19 AM
 */

namespace App\Models;


use CodeIgniter\Model;

class ConductorModel extends Model
{


    protected $table      = 'conductores';
    protected $returnType = 'object';
    protected $primaryKey = 'codigo';

    protected $allowedFields = ['identificacion','nombres','apellidos', 'fechaNacimiento','activo','licencia','categoriaLicencia','vigenciaLicencia','fechaRegistro','ultimaFechaActualizacion'];


    function consultar($identificacion=null, $activo=null){


        if (!is_null($identificacion)){


            $this->where("identificacion",$identificacion);


        }

        !is_null($activo)? $this->where("activo",1):'';


        return $this->orderBy('nombres','asc')->first();


    }


	function consultarTodo(){


		return $this->orderBy('nombres','asc')->findAll();


	}

    function filtrar($nombre){


    	return $this->like('nombres',$nombre,'both')->orLike("apellidos",$nombre,'both')->findAll();


	}




}
