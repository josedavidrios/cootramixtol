<?php
/**
 * Created by PhpStorm.
 * Administrador: jose.riosp
 * Date: 26/11/2019
 * Time: 10:19 AM
 */

namespace App\Models;


use CodeIgniter\Model;

class DespachoModel extends Model
{


    protected $table      = 'despachos';
    protected $returnType = 'object';
    protected $primaryKey = 'codigo';

    protected $allowedFields = ['ruta','placaVehiculo','hora', 'fecha','numeroPasajeros','fechaRegistro'];



	function consultar($placa=null, $fecha=null):array{


		$this->select("despachos.numeroPasajeros, hora, rutas.nombre AS nombreRuta")
			->join("rutas","rutas.codigo=despachos.ruta","INNER")
			->orderBy("hora","ASC");

		if (!is_null($placa)){
			$this->where("placaVehiculo",$placa);


		}

		if (!is_null($fecha)){

			$this->where("fecha",$fecha);
		}


		return $this->findAll();



	}


    function consultarPorFechas($placa=null, $fechaInico, $fechaFin):array{


        $this->select("SUM(numeroPasajeros) AS numeroPasajeros, despachos.fecha, conductores.nombres as conductor ")
			->where("despachos.fecha>=",$fechaInico)
			->where("despachos.fecha<=",$fechaFin)
			->join("vehiculos","vehiculos.placa = despachos.placaVehiculo","INNER")
			->join("planillas","planillas.placaVehiculo = despachos.placaVehiculo AND planillas.fecha = despachos.fecha","INNER")
			->join("conductores","conductores.identificacion = planillas.identificacionConductor","INNER")
			->groupBy("despachos.fecha,conductores.nombres")
			->orderBy("despachos.fecha","ASC");


        if (!is_null($placa)){
            $this->where("despachos.placaVehiculo",$placa);


        }



        return $this->findAll();



    }



}
