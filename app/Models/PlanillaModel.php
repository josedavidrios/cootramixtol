<?php

namespace App\Models;
use CodeIgniter\Model;

class PlanillaModel extends Model
{
    protected $table = 'planillas';
    protected $returnType = 'object';
    protected $primaryKey = 'codigo';

    protected $allowedFields = ['fecha',
                                'hora',
                                'registradoPor',
                                'identificacionConductor',
                                'placaVehiculo',
                                'codigoTarifa',
                                'estado',
                                'fechaRegistro',
								'codigoTarifa',
								'ultimaFechaActualizacion'
                                ];


    function consultar($numero=null){

        if (!is_null($numero)){
            $this->where("numero",$numero);
        }
        return $this->findAll();
    }

	function cambiarEstado($numero,$estado):bool {

		return $this->set(['estado' => $estado,"ultimaFechaActualizacion"=>get_now()])
			->where(['numero' => $numero])
			->update();

	}

    function crear($dat){
        return $this->insert($dat);
    }

    function filtrar($placa):array{
			$this->select("numero,placaVehiculo,fecha,tarifas.total as tarifa,estadoplanillas.descripcion as estado")
				->join("estadoplanillas","estadoplanillas.codigo=planillas.estado","INNER")
				->join("tarifas","tarifas.codigo=codigoTarifa","INNER");
        return $this->like('placaVehiculo',$placa,'both')->findAll();
	}


	function consultarPor($campo="placa",$valor):array{

        $this->select("planillas.numero, planillas.fecha, r.nombre As ruta, c.nombres AS conductor")
			 ->join("detalleplanillas dp","dp.numeroPlanilla=planillas.numero","INNER")
			 ->join("rutas r","dp.codigoRuta=r.codigo","INNER")
        	 ->join("conductores c","c.identificacion=planillas.identificacionConductor","INNER")
             ->where($campo,$valor);
        return $this->findAll();
    }

	function consultarPorPlaca($placa,$fecha=null):array{

		$this->select("planillas.numero, planillas.fecha, r.nombre As ruta, CONCAT(c.nombres,' ',apellidos) AS conductor, placaVehiculo")
			->join("detalleplanillas dp","dp.numeroPlanilla=planillas.numero","INNER")
			->join("rutas r","dp.codigoRuta=r.codigo","INNER")
			->join("conductores c","c.identificacion=planillas.identificacionConductor","INNER")
			->where("placaVehiculo",$placa);

			!empty($fecha) ? $this->where("fecha",$fecha) : "";

		return $this->findAll();

	}

    function crearDetallePlanilla($dat){

        $db      = \Config\Database::connect();
        $builder = $db->table('detalleplanillas');
        $builder->insert($dat);
    }

}
