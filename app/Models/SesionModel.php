<?php
/**
 * Created by PhpStorm.
 * Administrador: jose.riosp
 * Date: 26/11/2019
 * Time: 10:19 AM
 */

namespace App\Models;


use CodeIgniter\Model;

class SesionModel extends Model
{


    protected $table = 'usuarios';
	protected $primaryKey = 'identificacion';
	protected $returnType = 'object';

    function consultarDatos($usuario){

		$this->select("identificacion,nombres,clave,rol, roles.descripcion as nombreRol")
   			->where("identificacion",$usuario)
			->join("roles","roles.codigo=rol","INNER")
			->where("activo",1);
        return $this->first();

    }

}
