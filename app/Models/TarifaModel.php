<?php

namespace App\Models;

use CodeIgniter\Model;

class TarifaModel extends Model
{

	protected $table      = 'tarifas';
	protected $returnType = 'object';
	protected $primaryKey = 'codigo';

	protected $allowedFields = ['gastosAdministracion',
		'aporteSocial',
		'fondoReposicion',
		'aportesGastosVarios',
		'total',
		'fechaRegistro',
		'aporteParqueadero',
		'activa'
	];

	function  consultarTarifasAnioActual():array{

		return $this->select('codigo, FORMAT(total,0) as total')->where("activa",1)->findAll();
	}

	function  consultar():array{
		return $this->findAll();
	}

	function inactivar($codigo):bool{


		return $this->set(['activa' => 0])
			->where(['codigo' => $codigo])
			->update();

	}

}
