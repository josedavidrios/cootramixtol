<?php
/**
 * Created by PhpStorm.
 * Administrador: jose.riosp
 * Date: 26/11/2019
 * Time: 10:19 AM
 */

namespace App\Models;


use CodeIgniter\Model;

class UsuarioModel extends Model
{


    protected $table      = 'usuarios';
    protected $returnType = 'object';
    protected $primaryKey = 'codigo';

    protected $allowedFields = ['identificacion',
		                        'nombres',
								'apellidos',
		                        'activo',
		                        'ultimaFechaActualizacion',
								'clave',
		                        'fechaRegistro',
								'rol',
								'correo'
		];


    function consultar($identificacion=null){

        if (!is_null($identificacion)){
            $this->where("identificacion",$identificacion);
        }

		$this->select("identificacion,apellidos,nombres, activo, codigo, correo");
        return $this->orderBy("activo","desc")->orderBy('nombres','asc')->first();

    }

	function consultarTodos(){


		$this->select("identificacion,apellidos,nombres, activo, codigo, correo");
        return $this->orderBy("activo","desc")->orderBy('nombres','asc')->findAll();

	}

    function filtrar($nombre){
		$this->select("identificacion,apellidos,nombres, activo, codigo, correo" );

    	return $this->where("rol","ASOC")->like('nombres',$nombre,'both')->orLike("apellidos",$nombre,"both")->findAll();

	}


	function cambiarClave($identificacion,$nuevaClave):bool
	{


		$data =[
			"clave"=>$nuevaClave,
			"ultimaFechaActualizacion"=>get_now()

		];

		return $this->set($data)->where(["identificacion"=>$identificacion])->update();

	}




}
