<?php
/**
 * Created by PhpStorm.
 * Administrador: jose.riosp
 * Date: 26/11/2019
 * Time: 10:19 AM
 */

namespace App\Models;


use CodeIgniter\Model;

class VehiculoAsociadoModel extends Model
{
	protected $table      = 'vehiculosasociados';
	protected $returnType = 'object';
	protected $primaryKey = 'codigo';


	public function consultarVehiculosPorAsociado($identificacion){

		$this->select("placaVehiculo")->where('identificacionAsociado',$identificacion);
		return $this->findAll();
	}

}
