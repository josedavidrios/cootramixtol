<?php
/**
 * Created by PhpStorm.
 * Administrador: jose.riosp
 * Date: 26/11/2019
 * Time: 10:19 AM
 */

namespace App\Models;


use CodeIgniter\Model;

class VehiculoModel extends Model
{


    protected $table      = 'vehiculos';
    protected $returnType = 'object';
    protected $primaryKey = 'codigo';

    protected $allowedFields = ['placa',
		                        'tipo',
								'marca',
		                        'identificacionConductorTitular',
								'tipoPlanitllaPorDefecto',
								'nombres',
								'modelo',
								'capacidadPasajeros',
								'activo',
								'fechaRegistro',
								'codigoTarifa',
								'ultimaFechaActualizacion',
								'tarjetaPropiedad',
								'vigenciaSoat',
								'vigenciaRtm'
	];


    function consultarHabilitados():array{

		$this->select("placa,identificacionConductorTitular,vehiculos.codigoTarifa")
			->join("conductores c","c.identificacion = vehiculos.identificacionConductorTitular","INNER")
			->where("vehiculos.activo",1)
			->where("vehiculos.vigenciaSoat>=",get_now())
			->where("c.vigenciaLicencia>=",get_now())
			->orderBy('vehiculos.placa','asc');

        return $this->findAll();

    }

	function consultarActivos():array{

		$this->select("placa,identificacionConductorTitular,vehiculos.codigoTarifa")
			->join("conductores c","c.identificacion = vehiculos.identificacionConductorTitular","INNER")
			->where("vehiculos.activo",1)
			->orderBy('vehiculos.placa','asc');

		return $this->findAll();

	}


	function consultar($placa){

		$this->select("placa,identificacionConductorTitular,vehiculos.codigoTarifa")
			->join("conductores c","c.identificacion = vehiculos.identificacionConductorTitular","INNER")
			->where("vehiculos.activo",1)
			->where("vehiculos.vigenciaSoat>=",get_now())
			->where("c.vigenciaLicencia>=",get_now())
			->where('placa',$placa)
			->orderBy('vehiculos.placa','asc');

		return $this->first();

	}

	function cambiarTarifa($placa, $tarifa):bool {

		return $this->set(['codigoTarifa' => $tarifa,"ultimaFechaActualizacion"=>get_now()])
			->where(['placa' => $placa])
			->update();

	}

	function consultarDetalles($placa){

		$this->select("vehiculos.codigo,placa,identificacionConductorTitular,codigoTarifa,marca,tipo,modelo, capacidadPasajeros,tarjetaPropiedad,vehiculos.activo,vigenciaSoat,vigenciaRtm")
			->join("conductores c","c.identificacion = identificacionConductorTitular","INNER")
			->where('placa',$placa)
			->orderBy('vehiculos.placa','asc');

		return $this->first();

	}



	function existe($placa): int{

		return $this->where("placa",$placa)->countAllResults();

	}


    function consultarInformacionConductor($placa=null):array{


        $this->select("vehiculos.placa, c.nombres, c.apellidos, vehiculos.activo")
			->join("conductores c","c.identificacion = vehiculos.identificacionConductorTitular","INNER")
			->orderBy("vehiculos.activo","DESC");
		//	->where("vehiculos.activo",1);


		if (!is_null($placa)){

			$this->where("placa",$placa);


		}


        return $this->findAll();

    }


    function filtrar($placa):array{


		$this->select("vehiculos.placa, c.nombres, c.apellidos,vehiculos.activo")
			->join("conductores c","c.identificacion = vehiculos.identificacionConductorTitular","INNER");

    	return $this->like('placa',$placa,'both')->findAll();


	}

	function consultarVigenciasVehiculos():array{


		$this->select("vehiculos.placa AS vehiculo, vehiculos.vigenciaSoat, c.vigenciaLicencia AS vigenciaLicenciaConductor, CONCAT(c.nombres,' ',c.apellidos) AS nombresConductor, vehiculos.vigenciaRtm")
			->join("conductores c","c.identificacion = vehiculos.identificacionConductorTitular","INNER")
			->where("vehiculos.activo",1)->orderBy("vehiculos.placa","DESC");

		return $this->findAll();

	}


}
