<div class="row">


    <div class="container-fluid">

        <div class="row">
            <div class="col-md-11 col-sm-11">
                <form class="navbar-left navbar-form nav-search mr-md-12">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <button type="submit" class="btn btn-search pr-1">
                                <i class="fa fa-search search-icon"></i>
                            </button>
                        </div>
                        <input type="text" @keyup="filtrar" v-model="filtro" placeholder="Buscar...."
                               class="form-control text-uppercase">

                    </div>
                </form>


            </div>

            <div class="col-md-1 col-sm-1">


                <a href="#" class="pull-right btn btn-info btn-border btn-round btn-sm" data-toggle="modal" @click="abrirModal(0)">
                    <span class="btn-label"><i class="fa fa-user-plus"></i></span>Nuevo
                </a>


            </div>

        </div>


    </div>


</div>

<br>

<div class="row">


    <div class="col-md-12">


        <div class="card">


            <!--
            <div class="card-header">
                <div class="card-title">Hoverable Table</div>
            </div>

            -->

            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th width="10" scope="col">#</th>
                        <th scope="col">IDENTIFICACIÓN</th>
                        <th scope="col">NOMBRES</th>
                        <th class="text-center"  scope="col">ACTIVO</th>
                        <th scope="col">EDITAR</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="(item, index) in listado">
                        <td>{{index+1}}</td>
                        <td>{{item.identificacion}}</td>
                        <td>{{item.nombres }} {{item.apellidos}}</td>

						<!-- v-html="item.activo" -->
                        <td class="text-center"> <span class="badge"  v-bind:class="{ 'badge-success': item.activo==1, 'badge-danger': item.activo==0 }" >{{item.activo==1?"Sí":"No"}}</span> </td>

                        <td width="10" class="text-center">

                            <a @click="abrirModal(item.identificacion)"> <i class="fas fa-edit"></i> </a>

                        </td>

                    </tr>

                    </tbody>
                </table>
            </div>
        </div>


    </div>


</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">


        <!--

        v-on:submit.prevent="registrarVehiculos"

        -->

        <form method="post" id="form" @submit.prevent="crear">

            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">


                    <div class="row">
                        <div class="col-md-12">


                            <div class="card-body">


                                <div class="form-group">
                                    <label>Identificación</label>

                                    <input type="hidden" v-model="asociado.codigo" required class="form-control text-uppercase">


                                    <input type="number" v-model="asociado.identificacion" class="form-control text-uppercase"
                                           placeholder="Identificación">



                                </div>

                                <div class="form-group">
                                    <label>Nombres</label>
                                    <input type="text" v-model="asociado.nombres" required class="form-control text-uppercase"
                                           placeholder="Nombres">

                                </div>

								<div class="form-group">
									<label>Apellidos</label>
									<input type="text" v-model="asociado.apellidos" required class="form-control text-uppercase"
										   placeholder="Apellidos">

								</div>

								<div class="form-group">
									<label>Correo</label>
									<input type="email" v-model="asociado.correo"  class="form-control text-uppercase"
										   placeholder="Correo">

								</div>

								<div class="form-group" v-if="operacion==='A'">
									<label>Activo </label>
									<select required v-model="asociado.activo"
											class="form-control text-uppercase" required>
										<option value="">SELECCIONE</option>
										<option value="1">Sí</option>
										<option value="0">NO</option>
									</select>
								</div>


                            </div>

                        </div>


                    </div>

                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Guardar">
				</div>

        </form>
    </div>
</div>



<script>



    const controlador = "Asociado/";

    new Vue({
        el: '#app',


        created: function () {

            this.listar();

        },

        data: {

            listado: [],
            filtro: '',
			operacion:'C',
            asociado:{

                codigo:0,
                identificacion: '',
                nombres: '',
				correo:'',
				apellidos:'',
                activo:0

            }



        },
        methods: {

            listar: function () {


                axios.get(BASE_URL +controlador+'mostrarTodos').then(response => {
                    this.listado = response.data;


                });

            },

            filtrar: function (event) {


              const params = new FormData();
              params.append('nombres', event.target.value);


                axios.post(BASE_URL + controlador+'filtrar', params).then(response => {

                    this.listado = response.data;
                });

            },


            consultar: function (identificacion) {

                const params = new FormData();
                params.append('identificacion', identificacion);

                axios.post(BASE_URL + controlador+'mostrar', params).then(response => {

                	this.asociado.codigo = response.data.codigo;
                    this.asociado.identificacion = response.data.identificacion;
                    this.asociado.nombres = response.data.nombres;
					this.asociado.apellidos = response.data.apellidos;
					this.asociado.correo = response.data.correo;
               		this.asociado.activo = response.data.activo ;


                });

            },
            crear: function (event) {


                const params = new FormData();


                var mjs="El asociado se ha registrado exitosamente";

                if (this.operacion==="A"){

                    mjs = "El asociado se ha actualizado exitosamente";

                    params.append('codigo', this.asociado.codigo);


                }

                params.append('identificacion', this.asociado.identificacion);
                params.append('nombres', this.asociado.nombres);
				params.append('correo', this.asociado.correo);
				params.append('apellidos', this.asociado.apellidos);
                params.append('activo', this.asociado.activo);


                axios.post(BASE_URL + controlador+'crear', params).then(response => {


                    if (response.data != "0") {

                        $('#exampleModal').modal('toggle');

                        swal({
                            title: "Mensaje",
                            text: mjs,
                            button: "Aceptar",
                        });

                        event.target.reset();
                    }


                    this.listar();


                    console.log(response.data);


                }).catch(error => {


                    swal({
                        title: "Mensaje",
                        text: "Error",
                        button: "Aceptar",
                        icon: 'error',
                    });

                });


            }, abrirModal: function (identificacion) {


				$('#form')[0].reset();

                if (identificacion!==0){

                    this.consultar(identificacion);
                    this.operacion='A';

					console.log(identificacion)


                }else{
					console.log("No")
				}

                $('#exampleModal').modal('toggle');

            },

        }
    });


</script>



