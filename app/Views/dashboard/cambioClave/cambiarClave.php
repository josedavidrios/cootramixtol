<div class="row">
    <div class="col-md-12">

		<form method="post" id="form" @submit.prevent="cambiarClave">

        <div class="card">
            <div class="card-header">
                <div class="card-title">Cambiar clave</div>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label >Clave actual</label>
                    <input type="password"  v-model="claveAnterior" class="form-control" placeholder="Ingrese aquí la clave actual">

                </div>
                <div class="form-group">
                    <label for="password">Nueva clave</label>
                    <input type="password" @keyup="compararClaseves"  v-model="nuevaClave" class="form-control" id="password" placeholder="Ingrese aquí la clave actual">
                </div>

				<div class="form-group">
					<label for="password2">Confirme la nueva clave</label>
					<input v-model="confirmacionNuevaClave"  @keyup="compararClaseves" type="password" class="form-control"   v-bind:class="{ 'is-invalid': !clavesIguales}" id="password2" placeholder="confirme aquí la nuevo clave">


					<div class="invalid-feedback">
						Las claves no coinciden
					</div>

				</div>

            </div>
            <div class="card-action pull-right">
                <button class="btn btn-success pull-right">Cambiar</button>

            </div>
        </div>
		</form>
    </div>

</div>


<script>


	const controlador = "Sesion/";

	new Vue({
		el: '#app',

		data: {

			claveAnterior:'',
			nuevaClave:'',
			confirmacionNuevaClave:'',
			clavesIguales: true


		},
		methods: {


			compararClaseves:function(){

				console.log(this.nuevaClave);
				console.log(this.confirmacionNuevaClave);

				this.clavesIguales = false;
				if (this.nuevaClave === this.confirmacionNuevaClave){
					this.clavesIguales = true
				}

			},
			cambiarClave: function (){



				const params = new FormData();

				params.append('claveAnterior', this.claveAnterior);
				params.append('nuevaClave', this.nuevaClave);

				axios.post(BASE_URL + controlador+'cambiarClave', params)

					.then(response => {

						window.location = `${BASE_URL}sesion/cerrar`;

					}).catch(error => {


					//window.location;

				} );




			},

		}
	});


</script>
