<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">Base Form Control</div>
            </div>
            <div class="card-body">


                <form action="<?=base_url('generar/planilla')?>" target="_blank" method="post">

                <div class="form-group form-inline">
                    <label for="inlineinput" class="col-md-1 col-form-label">Vehúculo</label>
                    <div class="col-md-8 p-0">

                        <v-select   :required="!selected"    :required="re"   v-model="selected"  :options="listado"></v-select>
                        <input type="hidden" name="placa" required v-model="selected" />



                    </div>

					<div class="col-md-2 p-2">
						<input type="date" name="fecha" class="form-control" >
					</div>
                    <div class="col-md-1 p-2">
                        <input type="submit" class="btn btn-success" value="Descargar">
                    </div>

                </div>


               </form>

            </div>

        </div>

    </div>

</div>
