<div class="row">


    <div class="container-fluid">

        <div class="row">
            <div class="col-md-5 col-sm-11">
                <form class="navbar-left navbar-form nav-search mr-md-12">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <button type="submit" class="btn btn-search pr-1">
                                <i class="fa fa-search search-icon"></i>
                            </button>
                        </div>
                        <input type="text" @keyup="filtrar" v-model="filtro" placeholder="Buscar...."
                               class="form-control text-uppercase">

                    </div>

                </form>




            </div>

			<div class="col-md-1 col-sm-1">
				<input type="date" @keyup="filtrar" v-model="fecha"
					   class="form-control">

			</div>

            <div class="col-md-1 col-sm-1">


                <a href="#" class="pull-right btn btn-info btn-border btn-round btn-sm" data-toggle="modal" @click="abrirModal()">
                    <span class="btn-label"><i class="fa fa-user-plus"></i></span>Nuevo
                </a>


            </div>

        </div>


    </div>


</div>

<br>

<div class="row">


    <div class="col-md-12">


        <div class="card">


            <!--
            <div class="card-header">
                <div class="card-title">Hoverable Table</div>
            </div>

            -->

            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th width="20" scope="col">#</th>
                        <th scope="col">NUM</th>
                        <th scope="col">VEHÍCULO</th>
                        <th scope="col">TARIFA</th>
                        <th scope="col">FECHA</th>
                        <th class="text-center" scope="col">ESTADO</th>
                        <th scope="col">PAGAR</th>
						<th scope="col">ANULAR</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="(item, index) in listado">
                        <td>{{index+1}}</td>
                        <td>{{item.numero}}</td>
                        <td>{{item.placaVehiculo}}</td>
                        <td>{{item.tarifa}}</td>
                        <td>{{item.fecha}}</td>

                        <td class="text-center">

							<span class="badge"  v-bind:class="{ 'badge-success': item.estado=='PAGADA', 'badge-primary': item.estado=='POR PAGAR' ,'badge-danger': item.estado=='ANULADA' }" >{{item.estado}}</span>
						</td>

                        <td width="10" class="text-center">

                            <a @click="abrirModalPagarPlanilla(item.numero)"> <i class="fas fa-dollar-sign"></i> </a>

                        </td>

						<td width="10" class="text-center">

							<a @click="abrirModalAnularPlanilla(item.numero)"> <i class="fas fa-times-circle"></i> </a>

						</td>

                    </tr>

                    </tbody>
                </table>
            </div>
        </div>


    </div>


</div>


<div class="modal fade" id="mo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">

        <form method="post" id="form" @submit.prevent="crear">

            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">


                    <div class="row">
                        <div class="col-md-12">


                            <div class="card-body">

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" v-model="checked"   >
                                        <span class="form-check-sign">Rango de Fechas</span>
                                    </label>
                                </div>

                                <div class="form-group">
                                    <label>{{checked? "De" :"Fecha" }}</label>


                                    <input type="date" v-model="planilla.fechaInicio" class="form-control">



                                </div>


                                <div class="form-group" v-if="checked">
                                    <label>Hasta</label>
                                    <input type="date" v-model="planilla.fechaFin" required class="form-control">

                                </div>

                            </div>

                        </div>


                    </div>

                </div>
                <div class="modal-footer">


                    <input type="submit" class="btn btn-primary" value="Guardar">


        </form>
    </div>
</div>










<script>



    const controlador = "Planilla/";

    new Vue({
        el: '#app',

		created() {

			var fecha = new Date();
			this.fecha = fecha.toJSON().slice(0,10);
		},

        data: {

            listado: [],
            filtro: '',
            checked:'',

			fecha:'',

            planilla:{

                fechaInicio:'',
                fechaFin: '',


            }



        },
        methods: {

            filtrar: function () {


              const params = new FormData();

              if (this.filtro.length>2){

                  params.append('placa', this.filtro);

                  axios.post(BASE_URL + controlador+'filtrar', params).then(response => {

                      this.listado = response.data;
                  });
              }



            },


            consultar: function (identificacion) {

                const params = new FormData();
                params.append('identificacion', identificacion);

                axios.post(BASE_URL + controlador+'mostrar', params).then(response => {

                	this.conductor.codigo = response.data[0].codigo;
                    this.conductor.identificacion = response.data[0].identificacion;
                    this.conductor.nombres = response.data[0].nombres;
                    this.conductor.fechaNacimiento = response.data[0].fechaNacimiento;
                    this.conductor.activo = response.data[0].activo;

                    if (this.conductor.activo==0){

                        this.conductor.activo =0;

                    }



                });

            },anular:function(numero){

				const params = new FormData();
				params.append('numero', numero);
				params.append('estado', "-1");


				axios.post(BASE_URL + controlador+'cambiarEstado', params).then(response => {

					if (response.data != "0") {
						swal("La planilla se anuló exitosamente.");
						this.filtrar(numero);

					}

				}).catch(error => {

					swal({
						title: "Mensaje",
						text: "Error",
						button: "Aceptar",
						icon: 'error',
					});

				});

			},pagar:function(numero){

                const params = new FormData();
                params.append('numero', numero);
				params.append('estado', "1");

                axios.post(BASE_URL + controlador+'cambiarEstado', params).then(response => {

                    if (response.data != "0") {
                        swal("El pago se registró exitosamente.");
                        this.filtrar(numero);

                    }

                }).catch(error => {

                    swal({
                        title: "Mensaje",
                        text: "Error",
                        button: "Aceptar",
                        icon: 'error',
                    });

                });
            },
            crear: function (event) {
                const params = new FormData();
                params.append('fechaInicio', this.planilla.fechaInicio);

                if(this.checked){

                    params.append('fechaFin', this.planilla.fechaFin);
                }

                axios.post(BASE_URL + controlador+'crear', params).then(response => {

					console.log("Tada");
					console.table(typeof(response.data));

                    if (response.data !== 0) {



                        $('#exampleModal').modal('toggle');

                        swal({
                            title: "Mensaje",
                            text: `Se han se generado ${response.data} planillas exitosamente`,
                            button: "Aceptar",
                        });

                     //   event.target.reset();
                    }else {


						swal({
							title: "Mensaje",
							text: "No se encontraron vehículos para planillas",
							button: "Aceptar",
							icon: 'warning',
						});
					}


                //    this.listar();

                    this.checked = false;


                }).catch(error => {


                    swal({
                        title: "Mensaje",
                        text: "Error",
                        button: "Aceptar",
                        icon: 'error',
                    });

                });


            }, abrirModal: function () {

              $('#exampleModal').modal('toggle');


            }, abrirModalPagarPlanilla: function(numero) {
                swal("¿Estás seguro que desea registrar el pago de planilla? ", {
                    buttons: {
                        cancel: "Cancelar",
                        catch: {
                            text: "Aceptar",
                            value: "catch",
                        }

                    },
                }).then((value) => {
                        switch (value) {

                            case "catch":

                               this.pagar(numero);

                                break;

                        }
                    });

            },abrirModalAnularPlanilla: function(numero) {
				swal("¿Estás seguro que anular planilla? ", {
					buttons: {
						cancel: "Cancelar",
						catch: {
							text: "Aceptar",
							value: "catch",
						}

					},
				}).then((value) => {
					switch (value) {

						case "catch":

							this.anular(numero);

							break;

					}
				});

			}

        }
    });


</script>



