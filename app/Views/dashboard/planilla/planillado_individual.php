<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">Base Form Control</div>
            </div>
            <div class="card-body">


                <form method="post" id="form" @submit.prevent="crear">

                    <div class="form-group form-inline">


                        <label for="inlineinput" class="col-md-1 col-form-label">Fecha</label>
                        <div class="col-md-2 p-0">

                            <input type="date" class="form-control w-100" required v-model="planilla.fecha">


                        </div>

						<label for="inlineinput" class="col-md-1 col-form-label">Vehículos</label>
						<div class="col-md-2 p-0">

							<v-select :required="!planilla.placas"  @input="consultar(planilla.placa)" :required="re" name="placas"
									  v-model="planilla.placa"  :options="listadoPlacas"></v-select>


						</div>

                        <label for="inlineinput" class="col-md-1 col-form-label">Conductor</label>

						<div class="col-md-3">
							<select v-model="planilla.conductor"  class="form-control text-uppercase" required>

								<option value="">SELECCIONE</option>
								<option v-for="item in listadoConductores" :value="item.identificacion">{{item.nombres}} {{item.apellidos}}</option>


							</select>
						</div>


						<div class="form-check">
							<label class="form-check-label">
								<input v-model="planilla.tarifaCero" class="form-check-input" type="checkbox" value="">
								<span class="form-check-sign">Tarifa cero</span>
							</label>
						</div>

                        <div class="col-md-1 p-2">
                            <input type="submit" class="btn btn-success" value="Planillar">
                        </div>

                    </div>


                </form>

            </div>

        </div>

    </div>

</div>

