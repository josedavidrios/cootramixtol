<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<div class="card-title">Base Form Control</div>
			</div>
			<div class="card-body">


				<form method="post" id="form" @submit.prevent="crear">

					<div class="form-group form-inline">


						<label for="inlineinput" class="col-md-1 col-form-label">Tarifa</label>

							<div class="col-md-2 p-0">


								<select v-model="tarifa"
										class="form-control text-uppercase" required>


									<option value="">SELECCIONE</option>

									<option v-for="item in tarifas" :value="item.codigo">
										{{item.total}}
									</option>


								</select>


							</div>

						<div class="col-md-2 p-0">

							<div class="form-check">
								<label class="form-check-label">
									<input @change="seleccionarTodos" class="form-check-input" v-model="todos" type="checkbox" value="">
									<span class="form-check-sign">Aplicar a todos los vehículo</span>
								</label>
							</div>

						</div>

						<label for="inlineinput" class="col-md-1 col-form-label">Vehículos</label>
						<div class="col-md-4 p-0">

							<v-select  multiple=""  name="placas"
									  v-model="placas"  :options="listadoPlacas"></v-select>


						</div>


						<div class="col-md-1 p-2">
							<input type="submit" class="btn btn-success" value="Asignar planilla">
						</div>

					</div>


				</form>

			</div>

		</div>

	</div>

</div>

