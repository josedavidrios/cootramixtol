<div class="row">


	<div class="container-fluid">

		<div class="row">
			<div class="col-md-11 col-sm-11">
				<form class="navbar-left navbar-form nav-search mr-md-12">
					<div class="input-group">
						<div class="input-group-prepend">
							<button type="submit" class="btn btn-search pr-1">
								<i class="fa fa-search search-icon"></i>
							</button>
						</div>
						<input type="text" @keyup="" v-model="filtro" placeholder="Buscar...."
							   class="form-control text-uppercase">

					</div>
				</form>

			</div>

			<div class="col-md-1 col-sm-1">
				<a href="#" class="pull-right btn btn-info btn-border btn-round btn-sm" data-toggle="modal"
				   @click="abrirModal()">
					<span class="btn-label"><i class="fa fa-user-plus"></i></span>Nuevo
				</a>

			</div>

		</div>


	</div>


</div>

<br>


<div class="row">


	<div class="col-md-12">


		<div class="card">


			<!--
			<div class="card-header">
				<div class="card-title">Hoverable Table</div>
			</div>

			-->

			<div class="card-body">
				<table class="table table-hover">
					<thead>
					<tr>
						<th scope="col">#</th>
						<th  scope="col">CÓDIGO</th>
						<th class="text-right" scope="col">GASTOS ADMISTRACIÓN</th>
						<th class="text-right" scope="col">APORTE SOCIAL</th>
						<th class="text-right" scope="col">FONDO DE REPOSICIÓN</th>
						<th  class="text-right" scope="col">APORTES GASTOS VARIOS</th>
						<th class="text-right" scope="col">APORTE PARQUEADERO</th>
						<th class="text-right" scope="col">TOTAL</th>
						<th  scope="col">ACTIVA</th>
						<th  scope="col">INACTIVAR</th>

					</tr>
					</thead>
					<tr v-for="(item, index) in listado">
						<td>{{index+1}}</td>
						<td>{{item.codigo}}</td>
						<td class="text-right">{{ formatPrice(item.gastosAdministracion) }}</td>
						<td class="text-right">{{ formatPrice(item.aporteSocial) }}</td>
						<td class="text-right">{{ formatPrice(item.fondoReposicion) }}</td>
						<td class="text-right">{{ formatPrice(item.aportesGastosVarios) }}</td>
						<td class="text-right">{{ formatPrice(item.aporteParqueadero) }}</td>
						<td class="text-right">{{ formatPrice(item.total)}}</td>
						<td class="text-center"> <span class="badge"  v-bind:class="{ 'badge-success': item.activa==1, 'badge-danger': item.activa==0 }" >{{item.activa==1?"Sí":"No"}}</span> </td>
						<td width="10" class="text-center">

							<a @click="mostrarAlertaInactivarTarifa(item.codigo)"> <i class="fas fa-edit"></i> </a>

						</td>
					</tr>

					</tbody>

				</table>
			</div>
		</div>


	</div>


</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	 aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">


		<!--

		v-on:submit.prevent="registrarVehiculos"

		-->

		<form method="post" id="form" @submit.prevent="crear" >

			<div class="modal-content ">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">


					<div class="row">
						<div class="col-md-12">


							<div class="card-body">


								<div class="form-group">
									<label>Gastos de Admisnistración</label>

									<input @keyup="calcularTotal"  type="number" v-model="tarifa.gastosAdministracion"
										   class="form-control text-uppercase"
										   placeholder="Gastos de Admisnistración">


								</div>

								<div class="form-group">
									<label>Fondo de Reposición</label>
									<input @keyup="calcularTotal"  type="number" v-model="tarifa.fondoReposicion" required
										   class="form-control text-uppercase"
										   placeholder="Fondo de Reposición">

								</div>

								<div class="form-group">
									<label>Aporte Social</label>
									<input @keyup="calcularTotal"  type="number" v-model="tarifa.aporteSocial" required
										   class="form-control text-uppercase"
										   placeholder="Aporte Social">

								</div>



								<div class="form-group">
									<label>Aportes Gastos Varios </label>
									<input @keyup="calcularTotal"  type="number" v-model="tarifa.aportesGastosVarios" required
										   class="form-control text-uppercase"
										   placeholder="Aportes Gastos Varios">
								</div>

								<div class="form-group">
									<label>Aporte parqueadero </label>
									<input @keyup="calcularTotal"  type="number" v-model="tarifa.aporteParqueadero" required
										   class="form-control text-uppercase"
										   placeholder="Aporte parqueadero">
								</div>

								<div class="form-group">
									<label>Total</label>
									<input type="text" v-model="tarifa.total"  disabled
										   class="form-control text-uppercase"
										   placeholder="Total">
								</div>

								<div class="form-check form-check-inline">
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" v-model="tarifa.autoAplicar" id="customCheck1">
										<label class="custom-control-label" for="customCheck1">Aplicar a todos los vehículos activos</label>
									</div>

								</div>


							</div>

						</div>


					</div>

				</div>
				<div class="modal-footer">

					<input type="submit" class="btn btn-primary" value="Guardar">


		</form>
	</div>
</div>


<script>


	const controlador = "Tarifa/";

	new Vue({
		el: '#app',

		created: function () {
			this.listar();
		},

		data: {

			listado: [],
			filtro: '',
			operacion: 'R',
			tarifa: {

		//		codigo: '',
				gastosAdministracion: 0,
				aporteSocial: 0,
				fondoReposicion: 0,
				aportesGastosVarios: 0,
				aporteParqueadero:0,
				total: 0,
				activa: '',
				autoAplicar:''

			}


		},
		methods: {
			formatPrice(value) {
				let val = (value/1).toFixed(2).replace('.', ',')
				return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
			},

			calcularTotal:function (){

				this.tarifa.total=  parseInt(this.tarifa.gastosAdministracion)+
					                parseInt(this.tarifa.fondoReposicion)+
					                parseInt(this.tarifa.aporteSocial)+
					                parseInt(this.tarifa.aportesGastosVarios)+
									parseInt(this.tarifa.aporteParqueadero);

				this.tarifa.total = this.tarifa.total.toLocaleString('es-CO', {style: 'currency',currency: 'COP'});

			},mostrarAlertaInactivarTarifa:function (codigo){

				swal("¿Estás seguro que desea inactivar la tarifa? ", {
					buttons: {
						cancel: "Cancelar",
						catch: {
							text: "Aceptar",
							value: "catch",
						}

					},
				}).then((value) => {
					switch (value) {

						case "catch":

							this.inactivar(codigo);
							this.listar();

							break;

					}
				});

			},

			listar: function () {


				axios.get(BASE_URL + controlador + 'listar').then(response => {
					this.listado = response.data;
				});

			},

			crear: function (event) {

				const params = new FormData();


				var mjs = "La tarifa se ha registrado exitosamente";

				params.append('gastosAdministracion', this.tarifa.gastosAdministracion);
				params.append('aporteSocial', this.tarifa.aporteSocial);
				params.append('fondoReposicion', this.tarifa.fondoReposicion);
				params.append('aportesGastosVarios', this.tarifa.aportesGastosVarios);
				params.append('aporteParqueadero', this.tarifa.aporteParqueadero);

				params.append('autoAplicar', this.tarifa.autoAplicar);

				axios.post(BASE_URL + controlador + 'crear', params).then(response => {

					console.log(response.data);

					if (response.data != "0") {

						$('#exampleModal').modal('toggle');

						swal({
							title: "Mensaje",
							text: mjs,
							button: "Aceptar",
						});


					}


					this.listar();

					event.target.reset();
					this.operacion = "C";

					this.tarifa.gastosAdministracion=0;
					this.tarifa.aporteSocial=0;
					 this.tarifa.fondoReposicion=0;
					 this.tarifa.aportesGastosVarios=0;
					this.tarifa.total=0;



				}).catch(error => {


					swal({
						title: "Mensaje",
						text: "Error",
						button: "Aceptar",
						icon: 'error',
					});

				});


			},inactivar:function (codigo){


				const params = new FormData();

				var mjs = "La tarifa se ha inactivado exitosamente";

				params.append('codigo', codigo);


				axios.post(BASE_URL + controlador + 'inactivar', params).then(response => {

					console.log(response.data);

					if (response.data != "0") {


						swal({
							title: "Mensaje",
							text: mjs,
							button: "Aceptar",
						});


					}

				});
			},
			abrirModal: function () {


				$('#form')[0].reset();
				$('#exampleModal').modal('toggle');

			},

		}
	});

	Vue.filter('toCurrency', function (value) {
		if (typeof value !== "number") {
			return value;
		}
		var formatter = new Intl.NumberFormat('en-US', {
			style: 'currency',
			currency: 'COP'
		});
		return formatter.format(value);
	});


</script>

