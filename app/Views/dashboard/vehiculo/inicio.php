<div class="row">


	<div class="container-fluid">

		<div class="row">
			<div class="col-md-11 col-sm-11">
				<form class="navbar-left navbar-form nav-search mr-md-12">
					<div class="input-group">
						<div class="input-group-prepend">
							<button type="submit" class="btn btn-search pr-1">
								<i class="fa fa-search search-icon"></i>
							</button>
						</div>
						<input type="text" @keyup="filtar" v-model="filtro" placeholder="Buscar...."
							   class="form-control text-uppercase">

					</div>
				</form>


			</div>

			<div class="col-md-1 col-sm-1">


				<a href="#" class="pull-right btn btn-info btn-border btn-round btn-sm" data-toggle="modal"
				   @click="abrirModal(0)">
					<span class="btn-label"><i class="fa fa-user-plus"></i></span>Nuevo
				</a>


			</div>

		</div>


	</div>


</div>

<br>

<div class="row">


	<div class="col-md-12">


		<div class="card">

			<div class="card-body">
				<table class="table table-hover">
					<thead>
					<tr>
						<th width="10" scope="col">#</th>
						<th scope="col">PLACA</th>
						<th scope="col">CONDUCTOR</th>
						<th class="text-center" scope="col">ACTIVO</th>
						<th scope="col">EDITAR</th>
					</tr>
					</thead>
					<tbody>
					<tr v-for="(item, index) in listado">
						<td>{{index+1}}</td>
						<td>{{item.placa}}</td>
						<td>{{item.nombres}} {{item.apellidos}} </td>
						<td class="text-center"> <span class="badge"  v-bind:class="{ 'badge-success': item.activo==1, 'badge-danger': item.activo==0 }" >{{item.activo==1?"Sí":"No"}}</span> </td>

						<td width="10" class="text-center">

							<a @click="abrirModal(item.placa)"> <i class="fas fa-edit"></i> </a>

						</td>

					</tr>

					</tbody>
				</table>
			</div>
		</div>


	</div>


</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	 aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">


		<!--

		v-on:submit.prevent="registrarVehiculos"

		-->

		<form method="post" id="form" @submit.prevent="crear">

			<div class="modal-content ">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">


					<div class="row">
						<div class="col-md-12">


							<div class="card-body">

								<div class="row">

									<div class="col-md-6">
										<div class="form-group">

										<label>Placa</label>

										<input type="hidden" v-model="vehiculo.codigo"
											   class="form-control text-uppercase">

										<input type="text" v-model="vehiculo.placa" class="form-control text-uppercase"
											   placeholder="Placa" required>
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
										<label>Tarjeta de Propiedad</label>
										<input type="text" v-model="vehiculo.tarjetaPropiedad"
											   class="form-control text-uppercase"
											   placeholder="Tarjeta de Propiedad">
									</div>
									</div>


								</div>


								<div class="form-group">
									<label>Tipo </label>

									<select v-model="vehiculo.tipo" class="form-control text-uppercase" required>


										<option value="">SELECCIONE</option>

										<option v-for="item in tipos" :value="item">{{item}}</option>


									</select>


								</div>


								<div class="form-group">
									<label>Conductor </label>


									<select v-model="vehiculo.identificacionConductorTitular" class="form-control text-uppercase"
											required>


										<option value="">SELECCIONE</option>

										<option v-for="item in listadoConductores" :value="item.identificacion">
											{{item.nombres}} {{item.apellidos}}
										</option>


									</select>


								</div>

								<div class="form-group">

									<label>Vigencia SOAT </label>
									<input type="date" v-model="vehiculo.vigenciaSoat" class="form-control text-uppercase"
										   placeholder="Vigencia SOAT" required>

								</div>

								<div class="form-group">

									<label>Vigencia Revisión técnico mecánica  </label>
									<input type="date" v-model="vehiculo.vigenciaRtm" class="form-control text-uppercase"
										   placeholder="Vigencia Revisión técnico mecánica" required>

								</div>


								<div class="form-group">
									<label>Marca </label>


									<select v-model="vehiculo.marca" class="form-control text-uppercase" required>


										<option value="">SELECCIONE</option>

										<option v-for="item in marcas" :value="item">{{item}}</option>


									</select>


								</div>

								<div class="form-group">
									<label>Modelo </label>
									<input type="number"  v-model="vehiculo.modelo" class="form-control text-uppercase"
										   placeholder="Modelo" required>
								</div>

								<div class="form-group">
									<label>Capacidad de pasajeros </label>
									<input type="number" max="12" v-model="vehiculo.capacidadPasajeros"
										   class="form-control text-uppercase" placeholder="Capacidad de pasajeros">
								</div>


								<div class="form-group">
									<label>Tarifa </label>

									<select v-model="vehiculo.codigoTarifa"
											class="form-control text-uppercase" required>


										<option value="">SELECCIONE</option>

										<option v-for="item in tarifas" :value="item.codigo">
											{{item.total}}
										</option>


									</select>


								</div>


								<div class="form-group">
									<label>Activo </label>

									<select required v-model="vehiculo.activo"
											class="form-control text-uppercase" required>
										<option value="">SELECCIONE</option>
										<option value="1">Sí</option>
										<option value="0">NO</option>


									</select>


								</div>

							</div>

						</div>


					</div>

				</div>
				<div class="modal-footer">


					<input type="submit" class="btn btn-primary" value="Guardar">


		</form>
	</div>
</div>
</div>


<script>


	const controlador = "Vehiculo/";

	new Vue({
		el: '#app',


		created: function () {

			this.listar();
			this.listarConductores();
			this.listarTarifas();

		},

		data: {


			marcas: ['NISSAN', 'JEEP', 'TOYOTA'],
			tipos: ['CAMPERO', 'CAMIONETA'],

			operacion: 'R',

			listado: [],
			listadoConductores: [],
			filtro: '',
			tarifas: [],

			isChecked:false,
			vehiculo: {

				codigo: '',
				placa: '',
				marca: '',
				tipo: '',
				modelo: '',
				capacidadPasajeros: '',
				tarjetaPropiedad: '',
				codigoTarifa: '',
				identificacionConductorTitular: '',
				vigenciaSoat: '',
				activo: "1",
				vigenciaRtm:''

			}


		},
		methods: {


			listarConductores: function () {

				const params = new FormData();


				params.append('activo', "1");

				axios.get(BASE_URL + 'Conductor/mostrar').then(response => {
					this.listadoConductores = response.data;
				});

			},

			listarTarifas: function () {


				axios.get(BASE_URL + 'Tarifa/mostrarTipos').then(response => {
					this.tarifas = response.data;
				});

			},


			listar: function () {

				axios.get(BASE_URL + controlador + 'consultarInformacionConductor').then(response => {
					this.listado = response.data;
				});


			},

			filtar: function () {


				const params = new FormData();
				if (this.filtro.length > 2) {

					params.append('placa', this.filtro);
					axios.post(BASE_URL + controlador + 'filtrar', params).then(response => {
						this.listado = response.data;
					});

				}

			},


			consultar: function (placa) {

				const params = new FormData();
				params.append('placa', placa);


				axios.post(BASE_URL + controlador + 'mostrar', params).then(response => {

					console.table(response.data);

					this.vehiculo.codigo = response.data.codigo;
					this.vehiculo.placa = response.data.placa;
					this.vehiculo.tarjetaPropiedad = response.data.tarjetaPropiedad;
					this.vehiculo.marca = response.data.marca;
					this.vehiculo.tipo = response.data.tipo;
					this.vehiculo.modelo = response.data.modelo;
					this.vehiculo.capacidadPasajeros = response.data.capacidadPasajeros;
					this.vehiculo.codigoTarifa = response.data.codigoTarifa;
					this.vehiculo.identificacionConductorTitular = response.data.identificacionConductorTitular;
					this.vehiculo.activo = response.data.activo;
					this.vehiculo.vigenciaSoat = response.data.vigenciaSoat;
					this.vehiculo.vigenciaRtm = response.data.vigenciaRtm;




				});

			},
			crear: function (event) {


				const params = new FormData();
				var mjs = "El vehículo se ha registrado exitosamente";

				if (this.operacion === "A") {
					mjs = "El vehículo se ha actualizado exitosamente";

					console.log(this.vehiculo.codigo);

					params.append('codigo', this.vehiculo.codigo);
				}


				console.log(this.vehiculo.codigoTarifa);

				params.append('placa', this.vehiculo.placa);
				params.append('marca', this.vehiculo.marca);
				params.append('tipo', this.vehiculo.tipo);
				params.append('modelo', this.vehiculo.modelo);
				params.append('capacidadPasajeros', this.vehiculo.capacidadPasajeros);
				params.append('tarjetaPropiedad', this.vehiculo.tarjetaPropiedad);
				params.append('codigoTarifa', this.vehiculo.codigoTarifa);
				params.append('identificacionConductorTitular', this.vehiculo.identificacionConductorTitular);
				params.append('vigenciaSoat', this.vehiculo.vigenciaSoat);
				params.append('activo', this.vehiculo.activo);
				params.append('vigenciaRtm', this.vehiculo.vigenciaRtm);

				axios.post(BASE_URL + controlador + 'crear', params).then(response => {

					if (response.data !== "0") {


						$('#exampleModal').modal('toggle');

						swal({
							title: "Mensaje",
							text: mjs,
							button: "Aceptar",
						});

						event.target.reset();
					}


					this.operacion = "C";

					this.listar();


				}).catch(error => {


					swal({
						title: "Mensaje",
						text: "Error",
						button: "Aceptar",
						icon: 'error',
					});

				});


			}, abrirModal: function (placa) {



				if (placa !== 0) {
					this.consultar(placa);

					this.operacion = "A";

				} else {

					this.limpiarObjeto();
					$('#form')[0].reset();

				}


				$('#exampleModal').modal('toggle');

			},limpiarObjeto:function (){

				for (var clave in this.vehiculo) {
					if (this.vehiculo.hasOwnProperty(clave)) {
						this.vehiculo[clave] = "";
					}
				}

			}

		}
	});


</script>



