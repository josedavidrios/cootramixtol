Vue.component('v-select', VueSelect.VueSelect);

const controlador = "Despacho/";

new Vue({
    el: '#app',

    created: function () {
        this.listarPlacas();

    },

    data: {

        total:0,
        placa:'',
        fecha:'',
        listado: [],
        despachos:[]
    },


    methods: {

        listarPlacas: function () {

            axios.get(`${BASE_URL}/Vehiculo/mostrarPlacas`).then(response => {

                this.listado = response.data;

            });

        },

        consultar:function () {


            const params = new FormData();

            params.append('placa', this.placa);
            params.append('fecha', this.fecha);

            total=0;

            axios.post(`${BASE_URL}${controlador}/consultar`, params).then(response => {
                this.despachos = response.data;
                this.total = this.despachos.reduce((acum,actual)=>acum+ parseInt(actual.numeroPasajeros),0);

            }).catch(error => {


                swal({
                    title: "Mensaje",
                    text: "Error",
                    button: "Aceptar",
                    icon: 'error',
                });

            });




        }


    }
});