Vue.component('v-select', VueSelect.VueSelect);

const controlador = "Despacho/";

new Vue({
    el: '#app',

    created: function () {

        this.listarPlacas();

    },

    data: {

        total:0,
        placa:'',
        fechaInicio:'',
        fechaFin:'',
        listado: [],
        despachos:[]
    },


    methods: {

        listarPlacas: function () {

            axios.get(`${BASE_URL}vehiculo/mostrarPlacas`).then(response => {
                this.listado = response.data;

                console.log(response.data);

            });

        },

        consultar:function () {


            const params = new FormData();

            params.append('placa', this.placa);
            params.append('fechaInicio', this.fechaInicio);
            params.append('fechaFin', this.fechaFin);
            axios.post(`${BASE_URL}${controlador}/consultarPorFecha`, params).then(response => {
                this.despachos = response.data;
                this.total = this.despachos.reduce((acum,actual)=>acum+ parseInt(actual.numeroPasajeros),0);

                console.table(response);

            }).catch(error => {
                swal({
                    title: "Mensaje",
                    text: "Error",
                    button: "Aceptar",
                    icon: 'error',
                });

            });




        }


    }
});