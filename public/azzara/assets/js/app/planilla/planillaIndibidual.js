Vue.component('v-select', VueSelect.VueSelect);


const controlador = "Vehiculo/";

new Vue({
    el: '#app',

    created: function () {

        this.listarPlacas();
        this.listarConductores();


    },

    data: {
        planilla:{
            fecha:'',
            placa:'',
            conductor:'',
            tarifaCero:false
        },
        filtro: '',
        listadoPlacas: [],
        listadoConductores: []
    },

    methods: {

        listarPlacas: function () {

            axios.get(BASE_URL + controlador + 'mostrarPlacas').then(response => {
                this.listadoPlacas = response.data;
            });
        },
        listarConductores: function () {

            axios.get(BASE_URL + "conductor/" + 'mostrar').then(response => {
                this.listadoConductores = response.data;
            });
        },
        consultar: function (placa) {

            const params = new FormData();
            params.append('placa', placa);

            axios.post(BASE_URL + "Vehiculo/" + 'mostrar', params).then(response => {

                this.planilla.conductor = response.data.identificacionConductorTitular;
            });

        }, crear:function () {

            const params = new FormData();
            params.append('fecha', this.planilla.fecha);
            params.append('placa', this.planilla.placa);
            params.append('conductor', this.planilla.conductor);
            params.append('tarifaCero', this.planilla.tarifaCero);


            console.table(params);

            axios.post(BASE_URL +"Planilla/"+'individual', params).then(response => {

                if (response.data !== 0){


                    swal({
                        title: "Mensaje",
                        text: `Se ha creado la planlla para el vehículo ${this.planilla.placa} para la fecha ${this.planilla.fecha}`,
                        button: "Aceptar",
                    });

                }

            }).catch(error => {


                swal({
                    title: "Mensaje",
                    text: "Error",
                    button: "Aceptar",
                    icon: 'error',
                });

            });




        }


    }
});