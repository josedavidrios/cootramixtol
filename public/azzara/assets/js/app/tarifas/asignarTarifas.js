Vue.component('v-select', VueSelect.VueSelect);


const controlador = "Vehiculo/";

new Vue({
    el: '#app',

    created: function () {

        this.listarPlacas();
        this.listarTarifas();


    },

    data: {
        placas:[],
        listadoPlacas: [],
        tarifa:'',
        tarifas: [],
        todos:''
    },

    methods: {

        listarPlacas: function () {

            axios.get(BASE_URL + controlador + 'mostrarPlacas').then(response => {
                this.listadoPlacas = response.data;
            });
        },
        listarTarifas: function () {

            axios.get(BASE_URL + 'Tarifa/mostrarTipos').then(response => {
                this.tarifas = response.data;
            });
        }, seleccionarTodos: function(){

            if (this.todos){

                
            }

        }, crear:function () {

            const params = new FormData();
           params.append('tarifa', this.tarifa);
            params.append('placas', this.placas);
            params.append('todos', this.todos);

            axios.post(BASE_URL +"Tarifa/"+'asignarTarifaVehiculos', params).then(response => {

                if (response.data !== 0){
                    swal({
                        title: "Mensaje",
                        text: `Se ha actualizado la tarifa para  ${response.data} vehiculos`,
                        button: "Aceptar",
                    });

                }



            }).catch(error => {


                swal({
                    title: "Mensaje",
                    text: "Error",
                    button: "Aceptar",
                    icon: 'error',
                });

            });




        }


    }
});